//
//  AppDelegate+Helper.swift
//  DaKa
//
//  Created by Shun on 2018/12/25.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit

extension AppDelegate{
    
    func initValue(){
        if !UserDefaultsHelper.shared.has(.More_Current_Proximity) {
               UserDefaultsHelper.shared.set(MPLocationManagerProximityEnum.All.rawValue, for: .More_Current_Proximity)
        }
        
        if !UserDefaultsHelper.shared.has(.Last_Enter_Region) {
            UserDefaultsHelper.shared.set(false, for: .Last_Enter_Region)
        }
        
    }
    
    public func initRoot(){
        self.window? = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = .white
        let root = RootTBC()
        self.window?.rootViewController = root
        self.window?.makeKeyAndVisible()
    }
    
    public func requestAuthorization(){
        authorizationNotification()
    }
}

