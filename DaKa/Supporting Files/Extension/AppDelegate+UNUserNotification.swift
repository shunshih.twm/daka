//
//  AppDelegate+UNUserNotification.swift
//  DaKa
//
//  Created by Shun on 2018/12/25.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit
import UserNotifications

extension AppDelegate{
    
    func authorizationNotification(){
        // 在程式一啟動即詢問使用者是否接受圖文(alert)、聲音(sound)、數字(badge)三種類型的通知
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge, .carPlay], completionHandler: {
                [weak self] (granted, error) in
                if granted {
                    self?.print("允許")
                } else {
                    self?.print("不允許")
                }
            })
        } else {
            // Fallback on earlier versions
        }
    }
    
    func sendNotification(title: String, subtitle: String, body: String ){
        
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.title = title
            content.subtitle = subtitle
            content.body = body
            content.badge = 1
            content.sound = UNNotificationSound.default
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1.0 , repeats: false)
            
            let request = UNNotificationRequest(identifier: "notification", content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request, withCompletionHandler: {error in
                self.print("\(title) - 成功建立通知... ")
            })
        } else {
            // Fallback on earlier versions
        }
    }
    
    func initUNUserNotificationCenter(){
        // 代理 UNUserNotificationCenterDelegate，這麼做可讓 App 在前景狀態下收到通知
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
    }
    
    func setUNUserNotificationCenterDelegate(){
        // 代理 UNUserNotificationCenterDelegate，這麼做可讓 App 在前景狀態下收到通知
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate{
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("在前景收到通知...")
        completionHandler([.badge, .sound, .alert])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        completionHandler()
    }
}
