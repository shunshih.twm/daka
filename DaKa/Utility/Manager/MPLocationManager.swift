//
//  MPLocationManager.swift
//  DaKa
//
//  Created by Shun on 2018/12/25.
//  Copyright © 2018 com.terntek. All rights reserved.
//


import Foundation
import CoreLocation

protocol MPLocationManagerProtocol: class {
    func mPLocationManagerDidChangeAuthorization(status: MPLocationManagerStatusEnum)
    func mPLocationManagerDidEnterRegion()
    func mPLocationManagerDidEnterWithRegion(entitys: [MPLocationManagerEntity])
    func mPLocationManagerDidExitRegion()
    func mPLocationManagerStopRanging()
    func mPLocationManagerUpdate(entitys: [MPLocationManagerEntity])
}

enum MPLocationManagerStatusEnum {
    case MonitoringIsNotSupported
    case NeedRequestAuthorization
    case Authorized
    case Unknown
    case Denied
}

enum MPLocationManagerProximityEnum: Int, Codable {

    case VeryClose
    case Near
    case Far
    case Unknow
    case All

}

struct  MPLocationManagerEntity: Comparable, Hashable{
    
    let UUID: String
    let major: NSNumber
    let minor: NSNumber
    let proximity: MPLocationManagerProximityEnum
    
    let accuracy: Double
    let rssi: Int
    
    static func == (lhs: MPLocationManagerEntity, rhs: MPLocationManagerEntity) -> Bool {
        return lhs.UUID == rhs.UUID &&  lhs.major == rhs.major && lhs.minor == rhs.minor
    }
    
    static func < (lhs: MPLocationManagerEntity, rhs: MPLocationManagerEntity) -> Bool {
        return lhs.UUID == rhs.UUID &&  (lhs.major.intValue <  rhs.major.intValue || lhs.minor.intValue < rhs.minor.intValue)
    }
}


class MPLocationManager: NSObject{
    // MARK: - Type Alias
    
    // MARK: - Property
    weak var delegate: MPLocationManagerProtocol?
    
    lazy var locationManager: CLLocationManager = {
        let _CLLocationManager = CLLocationManager()
        _CLLocationManager.delegate = self
        // 開啟背景更新(預設為 false)
        _CLLocationManager.allowsBackgroundLocationUpdates = true
        // 不間斷的在背景更新(預設為 true)
        _CLLocationManager.pausesLocationUpdatesAutomatically = false
        return _CLLocationManager
    }()
    
    lazy var uuidString = {
        // for test
        //return "18738CC4-B99F-4B42-B6CC-F94CF7606EF7"
        return "44F426DD-B436-46A5-9399-328366D78FBC"
    }()
    
    lazy var identifier = {
        // for test
        return "Region1"
    }()
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    
    init(uuidString: String? = nil, identifier: String? = nil) {
        super.init()
        
        // default value
        self.uuidString = uuidString ??  self.uuidString
        self.identifier = identifier ??  self.identifier
    }
    
    // MARK: - Life Cycle
    deinit {
        print("shun - \( type(of: self) ) deinit")
    }
    
    // MARK: - Private Func
    private func isAuthorizationLocation() -> MPLocationManagerStatusEnum{
        var result: MPLocationManagerStatusEnum = .MonitoringIsNotSupported
        // Make sure region monitoring is supported.
        if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self){
            // Make sure the app is authorized.
            if  CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied{
                result = .Denied
            }else if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways{
                result = .NeedRequestAuthorization
            }else{
                result = .Authorized
            }
        }else{
            result = .MonitoringIsNotSupported
        }
        return result
    }
    
    private func registerBeaconRegionWithUUID(isMonitor: Bool){
        guard let UUID = UUID(uuidString: self.uuidString) else{
            print("uuid is nil")
            return
        }
        
        let region = CLBeaconRegion(proximityUUID:UUID, identifier: self.identifier)
        region.notifyOnEntry = true //預設就是true
        region.notifyOnExit = true //預設就是true
        
        if isMonitor{
            locationManager.startMonitoring(for: region) //建立region後，開始monitor region
        }else{
            locationManager.stopMonitoring(for: region)
            locationManager.stopRangingBeacons(in: region)
        }
    }

    // MARK: - Public Func
    func start(){
        let status = isAuthorizationLocation()
        if status == .Authorized{
            registerBeaconRegionWithUUID(isMonitor: true)
        }else{
            locationManager.requestAlwaysAuthorization()
        }
    }
}


// MARK: - CLLocationManagerDelegate
extension MPLocationManager: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if (status == CLAuthorizationStatus.denied) {
            print("didChangeAuthorization : denied")
            
            self.delegate?.mPLocationManagerDidChangeAuthorization(status: .Denied)
            
        } else if (status == CLAuthorizationStatus.authorizedAlways || status == CLAuthorizationStatus.authorizedWhenInUse) {
            print("didChangeAuthorization : authorizedAlways")
            
            self.delegate?.mPLocationManagerDidChangeAuthorization(status: .Authorized)
        }
    
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        print("MPLocationManager - didStartMonitoringFor/region : \(region)")
        manager.requestState(for: region)
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        print("MPLocationManager - didDetermineState/")
        
        if state == CLRegionState.inside{
            print("MPLocationManager - inside")
            
            if CLLocationManager.isRangingAvailable(){
                manager.startRangingBeacons(in: (region as! CLBeaconRegion))
                print("已在region中")
            }else{
                print("不支援ranging")
            }
        }else{
            print("MPLocationManager - outside")
            manager.stopRangingBeacons(in: (region as! CLBeaconRegion))
            self.delegate?.mPLocationManagerStopRanging()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("MPLocationManager - didEnterRegion")
        
        if CLLocationManager.isRangingAvailable(){
            manager.startRangingBeacons(in: (region as! CLBeaconRegion))
        }else{
            print("不支援ranging")
        }
        
        self.delegate?.mPLocationManagerDidEnterRegion()
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("MPLocationManager - didExitRegion")
        
        manager.stopRangingBeacons(in: (region as! CLBeaconRegion))
        
        self.delegate?.mPLocationManagerDidExitRegion()
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        print("MPLocationManager - didRangeBeacons/ counts : \(beacons.count)")
        
        //        proximity：與此iBeacon的距離，由近到遠分為immediate、near、far和unknown
        //
        //        accuracy：proximity的準確程度，單位是meter，數值越高表示proximity的準確程度越不能相信，負值表示無法計算。但千萬別把這個數值作為實際距離，此數值受到很多外在因素干擾，且距離越遠越無參考性。
        //
        //        rssi：接收到的iBeacon訊號強弱程度，單位為decibels
        
        var entitys = [MPLocationManagerEntity]()

        if (beacons.count > 0){

            for ( _, beacon ) in beacons.enumerated(){
                
                var proximityStatus: MPLocationManagerProximityEnum = .Unknow
                
                switch beacon.proximity {
                case CLProximity.immediate:
                    proximityStatus = .VeryClose
                    
                case CLProximity.near:
                    proximityStatus = .Near
                    
                case CLProximity.far:
                    proximityStatus = .Far
                    
                default:
                    proximityStatus = .Unknow
                }
                
                let entity = MPLocationManagerEntity.init(UUID: beacon.proximityUUID.uuidString, major: beacon.major, minor: beacon.minor, proximity: proximityStatus, accuracy: beacon.accuracy, rssi: beacon.rssi)
                
                entitys.append(entity)
                
                self.delegate?.mPLocationManagerDidEnterWithRegion(entitys: entitys)
            }
        }else{
            self.delegate?.mPLocationManagerDidExitRegion()
        }
        
        self.delegate?.mPLocationManagerUpdate(entitys: entitys)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("MPLocationManager - didFailWithError")
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("MPLocationManager - monitoringDidFailFor")
    }
    
    func locationManager(_ manager: CLLocationManager, rangingBeaconsDidFailFor region: CLBeaconRegion, withError error: Error) {
        print("MPLocationManager - rangingBeaconsDidFailFor")
    }
}


