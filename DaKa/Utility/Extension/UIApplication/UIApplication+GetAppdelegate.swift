//
//  UIApplication+GetAppdelegate.swift
//  DaKa
//
//  Created by Shun on 2018/12/25.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit

extension UIApplication{
    static func getAppdelegate() -> AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
}
