//
//  Date+Truncate.swift
//  DaKa
//
//  Created by Shun on 2019/1/8.
//  Copyright © 2019 com.terntek. All rights reserved.
//

import Foundation

extension Date{
    func truncateTime(components: Set<Calendar.Component>) -> Date? {

        return Calendar.current.date(from: Calendar.current.dateComponents(components, from: self))
    }
}
