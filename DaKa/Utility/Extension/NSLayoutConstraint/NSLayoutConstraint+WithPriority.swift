//
//  NSLayoutConstraint+WithPriority.swift
//  DaKa-Debug
//
//  Created by Shun on 2018/12/28.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
    /// Chainging a layout constraint with priority
    func withPriority(priority: UILayoutPriority) -> NSLayoutConstraint {
        self.priority = priority
        return self
    }
}
