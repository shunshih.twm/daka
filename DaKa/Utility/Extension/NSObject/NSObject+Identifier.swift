//
//  NSObject+Identifier.swift
//  DaKa
//
//  Created by Shun on 2018/12/25.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit

extension NSObject {
    static var identifier: String {
        return String(describing: type(of: self))
    }
}
