//
//  NSObject+Print.swift
//  DaKa-Debug
//
//  Created by Shun on 2018/12/25.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import Foundation

extension NSObject{
    
    public func print(_ items: Any..., separator: String = ",", terminator: String = "\n", function: String = #function) {
        
        #if DEBUG
        let output = items.map { "\($0)" }.joined(separator: separator)
        Swift.print("[ Called From: \( String(describing: type(of: self)))/\(function) ] = \(output)", terminator: terminator)
        #endif
    }
    
    public static func print(_ items: Any..., separator: String = ",", terminator: String = "\n", function: String = #function) {
        #if DEBUG
        let output = items.map { "\($0)" }.joined(separator: separator)
        Swift.print("[ Called From: \( String(describing: type(of: self)))/\(function) ] = \(output)", terminator: terminator)
        #endif
    }
}
