//
//  Thread+printCurrent.swift
//  DaKa-Debug
//
//  Created by Shun on 2019/1/14.
//  Copyright © 2019 com.terntek. All rights reserved.
//

import Foundation

extension Thread {
    public static func printCurrent() {
        print("\r⚡️: \(Thread.current)\r" + "🏭: \(OperationQueue.current?.underlyingQueue?.label ?? "None")\r")
    }
}
