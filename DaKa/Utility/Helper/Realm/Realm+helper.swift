//
//  Realm+helper.swift
//  DaKa-Debug
//
//  Created by Shun on 2019/1/3.
//  Copyright © 2019 com.terntek. All rights reserved.
//

import Foundation
import RealmSwift

extension Object{
    
    func saveData() -> Void {
        let realm = try! Realm()
        try! realm.write({
            // If update = true, objects that are already in the Realm will be
            // updated instead of added a new.
            realm.add(self, update: true)
        })
        
        print("realm fileURL: \(realm.configuration.fileURL!)")
    }
    
    func deleteData<K>(withPrimaryKey primaryKey: K){
        let realm = try! Realm()
        let data = realm.object(ofType: type(of: self), forPrimaryKey: primaryKey)
        
        guard let _data = data else {
            print("data is nil")
            return
        }
        
        try! realm.write {
            realm.delete(_data)
        }
        
    }
    
    func getData<T: Object, K>(ofType: T.Type, withPrimaryKey primaryKey: K, completion: ( ( _ data: T?) -> () )? ) {
        let realm = try! Realm()
    
        let result = realm.object(ofType: T.self, forPrimaryKey: primaryKey)
        completion?(result)
    }
    
    func getLastData<T: Object>(ofType: T.Type, predicate: NSPredicate , completion: ( ( _ data: T?) -> () )? ) {
        let realm = try! Realm()
        let result = realm.objects(T.self).filter(predicate)
        completion?(result.last)
    }
    
    func getAll<T: Object>(ofType: T.Type, completion: ( ( _ data: Results<T>?) -> () )? ) {
        let realm = try! Realm()
        let results = realm.objects(T.self)
        completion?(results)
    }
    
    func getCount<T: Object>(ofType: T.Type, predicate: NSPredicate , completion: ( ( _ count: Int) -> () )? ){
        let realm = try! Realm()
        let results = realm.objects(T.self).filter(predicate)
        completion?(results.count)
    }
}

