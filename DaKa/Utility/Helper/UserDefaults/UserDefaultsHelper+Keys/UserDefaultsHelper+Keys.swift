//
//  UserDefaultsHelper+Keys.swift
//  DaKa
//
//  Created by Shun on 2018/12/27.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import Foundation

extension UserDefaultsKey {
    static let More_Current_Proximity = Key<Int>("More_Current_Proximity")
    static let Last_Enter_Region = Key<Bool>("Last_Enter_Region")
}
