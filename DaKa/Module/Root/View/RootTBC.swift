//
//  RootTBC.swift
//  DaKa
//
//  Created by Shun on 2018/12/25.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit

class RootTBC: UITabBarController {
    // MARK: - Type Alias
    
    // MARK: - Property
    private var titles:[String]{
        return [ "History", "Detail", "More"]
    }
    
    lazy var vcs: [UIViewController] = {
        var _vcs = [UIViewController]()
        for (index, item)  in self.titles.enumerated() {
            
            var vc: UIViewController!
            if index == 0 {
                let navi = UINavigationController()
                navi.viewControllers = [HistoryVC()]
                vc = navi
            }else if index == 1 {
                vc = DetailVC()
            }else{
                vc = MoreVC()
            }
            
            vc.tabBarItem = UITabBarItem(title: item, image: UIImage.init(named: "\(item)"), tag: index)
            _vcs.append(vc)
            
        }
        return _vcs
    }()
    
    
    var viewModel: RootViewModel {
        return self.controller.viewModel
    }
    
    lazy var controller: RootController = {
        return RootController()
    }()
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        

        initView()
        initBinding()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.controller.start()
        
    }
    
    deinit {
        print("shun - deinit")
    }
    
    // MARK: - Private Func
    private func initView(){
        self.viewControllers = self.vcs
    }
    
    private func initBinding(){
        self.viewModel.locationManagerStatus.addObserver(fireNow: false) {
            [weak self] (status) in
            switch status {
            case .Authorized:
                self?.controller.start()
            case .MonitoringIsNotSupported:
                self?.presentAlertWithTitle(title: "", message: "Not Supported", options: "ok", completion: {
                    (option) in
                    // do nothing
                })
            case .NeedRequestAuthorization:
                self?.presentAlertWithTitle(title: "", message: "", options: "Need Request Authorization", completion: {
                    (option) in
                    // do nothing
                })
            case .Denied:
                self?.presentAlertWithTitle(title: "", message: "請先開啟位置權限", options: "ok", completion: {
                    (option) in
                    // do nothing
                })
            case .Unknown:
                self?.presentAlertWithTitle(title: "", message: "Unknown", options: "ok", completion: {
                    (option) in
                    // do nothing
                })
            }
        }
    }
    
    
    
    // MARK: - Public Func

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
