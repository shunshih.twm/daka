//
//  RootMPLocationManagerService.swift
//  DaKa
//
//  Created by Shun on 2018/12/26.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import Foundation

enum RootMPLocationManagerServiceEnum{
    enum Notification: String{
        case DidChangeAuthorization = "RootMPLocationManagerServiceNotificationDidChangeAuthorization"
        case Error = "RootMPLocationManagerServiceNotificationError"
        case DidEnterRegion = "RootMPLocationManagerServiceNotificationDidEnterRegion"
        case DidExitRegion = "RootMPLocationManagerServiceNotificationDidExitRegion"
        case Update = "RootMPLocationManagerServiceNotificationUpdate"
        case StopRanging = "RootMPLocationManagerServiceNotificationStopRanging"
    }
}

class RootMPLocationManagerService {
    // MARK: - Type Alias
    
    // MARK: - Property
    lazy var locationManager: MPLocationManager = {
        let _locationManager = MPLocationManager()
        _locationManager.delegate = self
        return _locationManager
    }()
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    
    // MARK: - Life Cycle
    deinit {
        print("shun - deinit")
    }
    
    // MARK: - Private Func
    
    // MARK: - Public Func
}

// MARK: - MPLocationManagerProtocol
extension RootMPLocationManagerService: MPLocationManagerProtocol{
    func mPLocationManagerStopRanging() {
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.StopRanging.rawValue), object: nil)
    }
    
    func mPLocationManagerDidChangeAuthorization(status: MPLocationManagerStatusEnum) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.DidChangeAuthorization.rawValue), object: status)
    }
    
    func mPLocationManagerDidEnterRegion() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.DidEnterRegion.rawValue), object: nil)
    }
    
    func mPLocationManagerDidEnterWithRegion(entitys: [MPLocationManagerEntity]) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.DidEnterRegion.rawValue), object: entitys)
    }
    
    func mPLocationManagerDidExitRegion() {
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.DidExitRegion.rawValue), object: nil)
    }
    
    func mPLocationManagerUpdate(entitys: [MPLocationManagerEntity]) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.Update.rawValue), object: entitys)
    }
}

