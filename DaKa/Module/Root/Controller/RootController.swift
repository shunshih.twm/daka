//
//  RootController.swift
//  DaKa
//
//  Created by Shun on 2018/12/26.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import Foundation
class RootController {
    
    // MARK: - Type Alias
    
    // MARK: - Property
    let viewModel: RootViewModel
    
    let service: RootMPLocationManagerService
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    init(viewModel: RootViewModel = RootViewModel(), service: RootMPLocationManagerService = RootMPLocationManagerService()) {
        self.viewModel = viewModel
        self.service = service
        
        initNotificationCenter()
    }
    
    
    // MARK: - Life Cycle
    deinit {
        remvoeNotificationCenter()
        print("shun - deinit")
    }
    
    // MARK: - Private Func
    private func initNotificationCenter(){
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onDidChangeAuthorization(_:)),
                                               name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.DidChangeAuthorization.rawValue),
                                               object: nil)
      
    }
    
    func remvoeNotificationCenter(){
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Public Func
    func start(){
        self.service.locationManager.start()
    }
 
}
