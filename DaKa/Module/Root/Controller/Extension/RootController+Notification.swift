//
//  RootController+Notification.swift
//  DaKa
//
//  Created by Shun on 2018/12/26.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit

extension RootController {
    
    @objc func onDidChangeAuthorization(_ notification: Notification){
        
        if let status = notification.object as? MPLocationManagerStatusEnum{
            self.viewModel.locationManagerStatus.value = status
        }
    }
    
    
   
    
}
