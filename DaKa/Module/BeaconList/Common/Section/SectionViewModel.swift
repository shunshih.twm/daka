//
//  SectionViewModel.swift
//  DaKa
//
//  Created by Shun on 2018/12/25.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import Foundation

struct SectionViewModel {
    let rowViewModels: [RowViewModel]
    let headerTitle: String
}
