//
//  DetailTVCell.swift
//  DaKa
//
//  Created by Shun on 2018/12/25.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit

class DetailTVCell: UITableViewCell, CellConfiguraable {
    // MARK: - Type Alias
    
    // MARK: - Property
    var viewModel: DetailTVCellViewModel?
    
    lazy var descLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(label)
        
        label.textColor = UIColor.darkGray
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(label)
        
        label.textColor = UIColor.lightGray
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        return label
    }()
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupInitialView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInitialView()
        setupConstraints()
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    deinit {
        print("shun - deinit")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    // MARK: - Private Func
    private func setupConstraints() {
        NSLayoutConstraint.activate([

            descLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            descLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            descLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10),
            descLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
            
            dateLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            dateLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10)
            ])
    }
    
    private func setupInitialView() {
        self.selectionStyle = .none
    }
    // MARK: - Public Func
    func setup(viewModel: RowViewModel) {
        guard let detailTVCellViewModel = viewModel as? DetailTVCellViewModel else { return }
        
        self.dateLabel.text = Date().formatString(dateFormat: "yyyy/MM/dd HH:mm:ss")
        self.descLabel.text = "\(detailTVCellViewModel.entity.major)/\(detailTVCellViewModel.entity.minor)\n\(detailTVCellViewModel.entity.UUID)\nproximity: \(detailTVCellViewModel.entity.proximity)\nrssi: \(detailTVCellViewModel.entity.rssi)"

    }

}
