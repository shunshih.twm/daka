//
//  DetailVC.swift
//  DaKa
//
//  Created by Shun on 2018/12/26.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    // MARK: - Type Alias
    
    // MARK: - Property
    
    var viewModel: DetailViewModel {
        return self.controller.viewModel
    }
    
    lazy var controller: DetailController = {
        return DetailController()
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        
        view.addSubview(tableView)
        var anchor: NSLayoutYAxisAnchor!
        var topConstant: CGFloat = 0.0
        if #available(iOS 11.0, *) {
            anchor = self.view.safeAreaLayoutGuide.topAnchor
        } else {
            anchor = self.view.topAnchor
            topConstant = 20.0
        }
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: anchor, constant:topConstant),
            tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0),
            tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
            tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0)
            ])
        
        tableView.register(DetailTVCell.self, forCellReuseIdentifier: DetailTVCell.identifier)
        return tableView
    }()
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initBinding()
        // Do any additional setup after loading the view.
    }
    
    deinit {
        print("shun - deinit")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.controller.start()
    }
    
    // MARK: - Private Func
    private func initView(){
        
    }
    
    private func initBinding() {
        self.viewModel.rowViewModels.addObserver(fireNow: true) {
            [weak self] (entitys) in
            self?.tableView.reloadData()
        }
    }
    
    // MARK: - Public Func


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


// MARK: - UITableViewDataSource
extension DetailVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.rowViewModels.value.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let rowViewModel = self.viewModel.rowViewModels.value[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: DetailTVCell.identifier, for: indexPath)
        
        if let cell = cell as? DetailTVCell{
            cell.setup(viewModel: rowViewModel )
        }
        
        return cell
    }
}
// MARK: - UITableViewDelegate
extension DetailVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
