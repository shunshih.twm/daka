//
//  DetailController.swift
//  DaKa
//
//  Created by Shun on 2018/12/25.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import Foundation

class DetailController {
    // MARK: - Type Alias
    
    // MARK: - Property
    let viewModel: DetailViewModel
    
    var dataSource: DetailDataSourceProtocol?
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    init(viewModel: DetailViewModel = DetailViewModel(), dataSource: DetailDataSourceProtocol = DetailDataSource() ) {
        self.viewModel = viewModel
        self.dataSource = dataSource
        initNotificationCenter()
    }
    
    // MARK: - Life Cycle
    deinit {
        remvoeNotificationCenter()
        print("shun - deinit")
    }
    
    // MARK: - Private Func
    
    private func initNotificationCenter(){
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onUpdate(_:)),
                                               name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.Update.rawValue),
                                               object: nil)
   
    }
    
    func remvoeNotificationCenter(){
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Public Func
    func start(){
        
    }
}
