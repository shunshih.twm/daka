//
//  DetailDataSource.swift
//  DaKa
//
//  Created by Shun on 2019/2/11.
//  Copyright © 2019 com.terntek. All rights reserved.
//

import UIKit

protocol DetailDataSourceProtocol {
    func detailDataSource(entitys: [MPLocationManagerEntity])  -> [RowViewModel]?
}

class DetailDataSource: DetailDataSourceProtocol {
    
    func detailDataSource(entitys: [MPLocationManagerEntity]) -> [RowViewModel]? {
        
        if let _statusRawValue = UserDefaultsHelper.shared.get(for: .More_Current_Proximity),
            let status = MPLocationManagerProximityEnum(rawValue: _statusRawValue){
            
            return entitys.filter({
                return $0.proximity.rawValue <= status.rawValue
            }).map({
                return DetailTVCellViewModel.init(entity: $0)
            })
        }
        
        return nil
    }
}
