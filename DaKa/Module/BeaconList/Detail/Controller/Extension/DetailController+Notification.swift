//
//  DetailController+Notification.swift
//  DaKa
//
//  Created by Shun on 2018/12/26.
//  Copyright © 2018 com.terntek. All rights reserved.
//


import UIKit

extension DetailController {
    
    @objc func onUpdate(_ notification: Notification){
        if let entitys = notification.object as? [MPLocationManagerEntity],
            let value = self.dataSource?.detailDataSource(entitys: entitys) {
            self.viewModel.rowViewModels.value = value
        }
    }
}
