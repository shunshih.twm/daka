//
//  HistoryDetailController.swift
//  DaKa-Debug
//
//  Created by Shun on 2019/1/17.
//  Copyright © 2019 com.terntek. All rights reserved.
//

import Foundation

class HistoryDetailController {
    // MARK: - Type Alias
    
    // MARK: - Property
    let viewModel: HistoryDetailViewModel
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    
    init(viewModel: HistoryDetailViewModel = HistoryDetailViewModel()) {
        self.viewModel = viewModel
    }
    // MARK: - Life Cycle
    deinit {
        print("shun - \( type(of: self) ) deinit")
    }
    
    // MARK: - Private Func
    
    
    
    // MARK: - Public Func
    func start() {
        
    }
    
}
