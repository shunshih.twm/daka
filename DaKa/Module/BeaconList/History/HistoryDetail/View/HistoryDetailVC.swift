//
//  HistoryDetailVC.swift
//  DaKa
//
//  Created by Shun on 2019/1/17.
//  Copyright © 2019 com.terntek. All rights reserved.
//

import UIKit

class HistoryDetailVC: UIViewController {
    // MARK: - Type Alias
    
    // MARK: - Property
    var viewModel: HistoryDetailViewModel {
        return controller.viewModel
    }
    
    lazy var controller: HistoryDetailController = {
        return HistoryDetailController()
    }()
    
    lazy var contentsView: [[UILabel: UILabel]] = {
        // title / content
        return [[UILabel: UILabel]]()
    }()
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initBinding()
        // Do any additional setup after loading the view.
    }
    
    deinit {
        print("shun - \( type(of: self) ) deinit")
    }
    
    // MARK: - Private Func
    private func initView() {
        view.backgroundColor = .white
        self.title = "History Deatail"
    }
    
    private func initBinding() {
        self.viewModel.contents.addObserver(fireNow: false, {[weak self] (contents) in
            self?.reloadContents()
        })
    }
    
    private func getLabel(isTitle: Bool, text: String) -> UILabel{
        let label = UILabel()
        self.view.addSubview(label)
        label.textColor = isTitle ? .black : .lightGray
        label.font = UIFont.systemFont(ofSize: 14.0)
        label.text = text
        return label
    }
    
    private func reloadContents(){
        removeContents()
        buildContents()
        addContents()
    }
    
    private func removeContents(){
        for dic in self.contentsView {
            dic.keys.first?.removeFromSuperview()
            dic.values.first?.removeFromSuperview()
        }
    }
    
    private func addContents(){
        let x: CGFloat = 40.0
        var y: CGFloat = 100.0
        let width: CGFloat = 100.0
        let height: CGFloat = 40.0
        let spacing: CGFloat = 20.0
        
        for (_, dic) in self.contentsView.enumerated() {
            let titleFrame = CGRect.init(x: x, y: y, width: width, height: height)
            dic.keys.first?.frame = titleFrame
            
            let contentX = titleFrame.origin.x + titleFrame.size.width + spacing
            let contentFrame = CGRect.init(x: contentX, y: y, width: width * 2, height: height)
            dic.values.first?.frame = contentFrame
            
            y += height + spacing
        }
    }
    
    private func buildContents(){
        // UUID
        if self.controller.viewModel.contents.value.uuid != ""{
            let uuidTitleLabel = getLabel(isTitle: true, text: "UUID")
            let uuidContentLabel = getLabel(isTitle: false, text: self.controller.viewModel.contents.value.uuid)
            uuidContentLabel.numberOfLines = 0
            self.contentsView.append([uuidTitleLabel: uuidContentLabel])
        }


        // Major Minor
        if self.controller.viewModel.contents.value.major != "" && self.controller.viewModel.contents.value.minor != ""{
            let majorMinorTitleLabel = getLabel(isTitle: true, text: "Major / Minor")
            let majorMinorContentLabel = getLabel(isTitle: false, text: "\(self.controller.viewModel.contents.value.major) / \(self.controller.viewModel.contents.value.minor)")
            self.contentsView.append([majorMinorTitleLabel: majorMinorContentLabel])
        }

        // proximity
        if self.controller.viewModel.contents.value.proximity != ""{
            let proximityTitleLabel = getLabel(isTitle: true, text: "proximity")
            let proximityContentLabel = getLabel(isTitle: false, text: "\(self.controller.viewModel.contents.value.proximity)")
            self.contentsView.append([proximityTitleLabel: proximityContentLabel])
        }
 
        // createDate
        let createDateTitleLabel = getLabel(isTitle: true, text: "Create Date")
        let createDateContentLabel = getLabel(isTitle: false, text: Date.init(timeIntervalSince1970: TimeInterval(self.controller.viewModel.contents.value.createDate)).formatString(dateFormat: "yyyy/MM/dd HH:mm:ss"))
        self.contentsView.append([createDateTitleLabel: createDateContentLabel])
        
        // isIn
        let isInTitleLabel = getLabel(isTitle: true, text: "Status")
        let isInContentLabel = getLabel(isTitle: false, text: self.controller.viewModel.contents.value.isIn ? "Enter" : "Leave")
        self.contentsView.append([isInTitleLabel: isInContentLabel])
    }
    
    // MARK: - Public Func
    func setup(entity: HistoryEntity){
        self.viewModel.contents.value = entity
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
