//
//  HistoryEntity.swift
//  DaKa
//
//  Created by Shun on 2018/12/28.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import Foundation
import RealmSwift

enum HistoryEntityPrimaryKeyEnum: String {
    case CreateDate = "createDate"
    case UUID = "uuid"
}

class HistoryEntity: Object{    // MARK: - Type Alias
    
    // MARK: - Property
    
    @objc dynamic var uuid = NSUUID().uuidString
    
    @objc dynamic var createDate: Int = Int(Date().timeIntervalSince1970)
    
    @objc dynamic var isIn: Bool = false

    @objc dynamic var UUID: String = ""
    
    @objc dynamic var major: String = ""
    
    @objc dynamic var minor: String = ""
    
    @objc dynamic var proximity: String = ""
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor

    // MARK: - Life Cycle
    deinit {
        print("shun - \( type(of: self) ) deinit")
    }
    
    // MARK: - Private Func
    internal func getToday() -> (fromTimestamp: Int, toTimestamp: Int)?{
        let currentDate = Date()
        let truncatedCurrentDate = currentDate.truncateTime(components: [.year, .month, .day])
        let toDate = truncatedCurrentDate?.add(years: 0, months: 0, days: 1, hours: 0, minutes: 0, seconds: 0)
        
        if let _fromDate = truncatedCurrentDate?.timeIntervalSince1970,
            let _toDate = toDate?.timeIntervalSince1970 {
            return (Int(_fromDate), Int(_toDate) )
        }else{
            return nil
        }
    }
    
    
    // MARK: - Public Func
 
    override class func primaryKey() -> String? {
        return HistoryEntityPrimaryKeyEnum.UUID.rawValue
    }
 
    // MARK: - DB Func
    static func getTodayIsInCount(completion: ( ( _ count: Int) -> () )? ){
       let entity = HistoryEntity.init()
        
        if let today = entity.getToday() {
            let predicate = NSPredicate.init(format: "createDate >= %d AND createDate < %d AND isIn == TRUE", today.fromTimestamp, today.toTimestamp)
           
            
            entity.getCount(ofType: HistoryEntity.self, predicate: predicate) {
                (count) in
                completion?(count)
            }
        }else{
            completion?(0)
        }
    }
    
    static func getAll(completion: ( ( _ data: Results<HistoryEntity>?) -> () )? ){
        let entity = HistoryEntity.init()
        entity.getAll(ofType: HistoryEntity.self , completion: {
            (entitys) in
            completion?(entitys)
        })
    }
    
    static func getTodayLastStatus(completion: ( ( _ isEnter: Bool?) -> () )? ){
        let entity = HistoryEntity.init()
        
        if let today = entity.getToday(){
            let predicate = NSPredicate.init(format: "createDate >= %d AND createDate < %d", today.fromTimestamp, today.toTimestamp)
            
            entity.getLastData(ofType: HistoryEntity.self, predicate: predicate, completion: {
                (entity) in
                
                guard let _entity = entity else{
                    completion?(false)
                    return
                }
                completion?(_entity.isIn)
            })
        }else{
            completion?(nil)
        }
    }
}

