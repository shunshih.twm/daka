//
//  HistoryController+Notification.swift
//  DaKa
//
//  Created by Shun on 2018/12/28.
//  Copyright © 2018 com.terntek. All rights reserved.
//


import UIKit

extension HistoryController {
    
    private func commonRegionFunc( isEnter: Bool){
        self.commonRegionFunc(isEnter: isEnter, entitys: nil)
    }
    
    private func commonRegionFunc( isEnter: Bool, entitys: [MPLocationManagerEntity]?){
        
        let isEnterRegion = UserDefaultsHelper.shared.get(for: .Last_Enter_Region)
        
        if isEnter{
            HistoryEntity.getTodayIsInCount(completion: {
                [weak self] (count) in
                
                
                if let _isEnterRegion = isEnterRegion{
                    
                    if !_isEnterRegion || count == 0 {
                        UserDefaultsHelper.shared.set(true, for: .Last_Enter_Region)
                        
                        self?.buildViewModels(isIn: true, entitys: entitys)
                        self?.showNotification()
                    }
                }
            })
 
        }else{
            if let _isEnterRegion = isEnterRegion,
                _isEnterRegion{
                
                UserDefaultsHelper.shared.set(false, for: .Last_Enter_Region)

                self.buildViewModels(isIn: false, entitys: entitys)
                // 2018/01/08 下班先不通知
                //sendNotification(title:  "下班打卡通知", body: "下班打卡時間: \(Date().formatString(dateFormat: "yyyy/MM/dd HH:mm:ss"))")
                
            }
            
        }
    }
    
    @objc func onDidEnterRegion(_ notification: Notification){
        
        if let entitys = notification.object as?  [MPLocationManagerEntity]{
            print("onDidEnterRegion: count: \(entitys.count)")
            commonRegionFunc(isEnter: true, entitys: entitys)
        }else{
            commonRegionFunc(isEnter: true)
        }
    }
    
    @objc func onDidExitRegion(_ notification: Notification){
        commonRegionFunc(isEnter: false)
    }
    
    @objc func onStopRanging(_ notification: Notification){
        // 2019/01/11
        // 範圍內關閉偵測,走出範圍外,打開偵測,需要補一筆離開
        commonRegionFunc(isEnter: false)
    }
    
    private func sendNotification(title:String, body: String){
        UIApplication.getAppdelegate()?.sendNotification(title: title, subtitle: "", body: body)
    }
    
    private func showNotification(){
        HistoryEntity.getTodayIsInCount(completion: {
            [weak self] (count) in
            
            if count == 1 {
                // 一天只顯示第一次通知
                self?.sendNotification(title:  "上班打卡通知", body: "上班打卡時間: \(Date().formatString(dateFormat: "yyyy/MM/dd HH:mm:ss"))")
            }
            
        })
    }
}
