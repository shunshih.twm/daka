//
//  HistoryDataSource.swift
//  DaKa
//
//  Created by Shun on 2019/2/11.
//  Copyright © 2019 com.terntek. All rights reserved.
//


import UIKit

protocol HistoryDataSourceProtocol {
    func historyDataSource(source: [SectionViewModel], rowViewModels: [HistoryEntity] ) -> [SectionViewModel]
}

class HistoryDataSource: HistoryDataSourceProtocol {
    
    func historyDataSource(source: [SectionViewModel], rowViewModels: [HistoryEntity] ) -> [SectionViewModel] {
   
        var sectionViewModels = [SectionViewModel]()
        sectionViewModels.append(contentsOf: source)
        
        for entity in rowViewModels{
            
            let currentHeaderTitle = Date(timeIntervalSince1970: TimeInterval(entity.createDate)).formatString(dateFormat: "yyyy/MM/dd")
            
            let cellViewModel = HistoryTVCellViewModel.init(entity: entity)
            cellViewModel.cellPressed = {
                if let navi = UIApplication.shared.keyWindow?.visibleViewController()?.parent as? UINavigationController{
                    let vc = HistoryDetailVC()
                    vc.setup(entity: entity)
                    navi.pushViewController(vc, animated: true)
                }
                print("cellPressed")
            }
            
            let sectionViewModel = SectionViewModel.init(rowViewModels: [cellViewModel], headerTitle:currentHeaderTitle )
            
            let isContain = sectionViewModels.contains {
                return $0.headerTitle == currentHeaderTitle
            }
            
            if isContain{
                
                sectionViewModels = sectionViewModels.map({
                    if $0.headerTitle == currentHeaderTitle{
                        var entitys = [RowViewModel]()
                        
                        entitys.append(contentsOf: $0.rowViewModels)
                        entitys.append(cellViewModel)
                        
                        return SectionViewModel.init(rowViewModels: entitys, headerTitle: currentHeaderTitle)
                    }else{
                        return $0
                    }
                })
                
            }else{
                sectionViewModels.append(sectionViewModel)
            }
        }
        return sectionViewModels
    }
}
