//
//  HistoryController.swift
//  DaKa-Debug
//
//  Created by Shun on 2018/12/28.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit


class HistoryController {
    // MARK: - Type Alias
    
    // MARK: - Property
    let viewModel: HistoryViewModel
    
    var dataSource: HistoryDataSourceProtocol?
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    init(viewModel: HistoryViewModel = HistoryViewModel(), dataSource: HistoryDataSourceProtocol = HistoryDataSource()) {
        self.dataSource = dataSource
        self.viewModel = viewModel
        initNotificationCenter()
    }
    
    // MARK: - Life Cycle
    deinit {
        remvoeNotificationCenter()
        print("shun - deinit")
    }
    
    // MARK: - Private Func
    
    private func initNotificationCenter(){
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onDidEnterRegion(_:)),
                                               name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.DidEnterRegion.rawValue),
                                               object: nil)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onDidExitRegion(_:)),
                                               name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.DidExitRegion.rawValue),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onStopRanging(_:)),
                                               name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.StopRanging.rawValue),
                                               object: nil)
        
    }
    
    // MARK: - Public Func
    func start(){
        HistoryEntity.getAll {
            [weak self] (entitys) in
            guard let _entitys = entitys,
            let sectionViewModels = self?.dataSource?.historyDataSource(source: [SectionViewModel](), rowViewModels: Array(_entitys)) else{
                print("HistoryEntity getData is nil")
                return
            }
     
            self?.viewModel.sectionViewModels.value = sectionViewModels
        }

    }
    
    func remvoeNotificationCenter(){
        NotificationCenter.default.removeObserver(self)
    }
    
    func buildViewModels(isIn: Bool, entitys: [MPLocationManagerEntity]?){
        
        //
        let entity = HistoryEntity.init()
        entity.createDate = Int(Date().timeIntervalSince1970)
        entity.isIn = isIn
        
        if let sourceEntity = entitys?.first {
            entity.major = "\(sourceEntity.major)"
            entity.minor = "\(sourceEntity.minor)"
            entity.proximity = "\(sourceEntity.proximity)"
        }
        
        // save
        entity.saveData()
 
        
        if let value = self.dataSource?.historyDataSource(source: self.viewModel.sectionViewModels.value, rowViewModels: [entity]){
          self.viewModel.sectionViewModels.value = value
        }
    }

    
}
