//
//  HistoryTVCell.swift
//  DaKa
//
//  Created by Shun on 2018/12/28.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit


class HistoryTVCell: UITableViewCell {
    // MARK: - Type Alias
    
    // MARK: - Property
    var viewModel: HistoryTVCellViewModel?
    
    lazy var descLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(label)
        
        label.textColor = UIColor.darkGray
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(label)
        
        label.textColor = UIColor.lightGray
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var statusImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(imageView)
        
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 3
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupInitialView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInitialView()
        setupConstraints()
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    deinit {
        print("shun - deinit")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        viewModel?.cellPressed = nil
    }
    
    // MARK: - Private Func
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            
            statusImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: 0),
            statusImageView.widthAnchor.constraint(equalToConstant: 20.0),
            statusImageView.heightAnchor.constraint(equalToConstant: 20.0),
            statusImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10),
            
            descLabel.topAnchor.constraint(equalTo: statusImageView.topAnchor, constant: 0),
            descLabel.bottomAnchor.constraint(equalTo: statusImageView.bottomAnchor, constant: 0),
            descLabel.leftAnchor.constraint(equalTo: statusImageView.rightAnchor, constant: 10),
            descLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
            
            dateLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            dateLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
            dateLabel.widthAnchor.constraint(equalToConstant: 70.0)
            ])
    }
    
    private func setupInitialView() {
        self.selectionStyle = .none
    }
    // MARK: - Public Func
    func setup(viewModel: RowViewModel) {
        guard let _viewModel = viewModel as? HistoryTVCellViewModel else { return }
        
        self.statusImageView.image = _viewModel.entity.isIn ? #imageLiteral(resourceName: "sign-in") : #imageLiteral(resourceName: "sign-out")
        self.dateLabel.text = Date(timeIntervalSince1970: TimeInterval(_viewModel.entity.createDate)).formatString(dateFormat: "HH:mm:ss")
        self.descLabel.text = _viewModel.entity.isIn ? "Enter" : "Leave"
    }
    
}
