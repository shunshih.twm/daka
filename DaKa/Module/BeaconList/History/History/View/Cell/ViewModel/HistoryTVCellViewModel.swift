//
//  HistoryTVCellViewModel.swift
//  DaKa
//
//  Created by Shun on 2019/1/16.
//  Copyright © 2019 com.terntek. All rights reserved.
//

import Foundation

class HistoryTVCellViewModel: RowViewModel, RowViewModelPressible {
    
    let entity: HistoryEntity

    var cellPressed: (() -> Void)?
    
    init(entity: HistoryEntity) {
        self.entity = entity

    }
}
