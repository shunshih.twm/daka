//
//  HistoryVC.swift
//  DaKa
//
//  Created by Shun on 2018/12/25.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController {
    // MARK: - Type Alias
    
    // MARK: - Property
    
    var viewModel: HistoryViewModel {
        return self.controller.viewModel
    }
    
    lazy var controller: HistoryController = {
        return HistoryController()
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        
        view.addSubview(tableView)
        
        var anchor: NSLayoutYAxisAnchor!
        var topConstant: CGFloat = 0.0
        if #available(iOS 11.0, *) {
            anchor = self.view.safeAreaLayoutGuide.topAnchor
        } else {
            anchor = self.view.topAnchor
            topConstant = 20.0
        }
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: anchor, constant:topConstant),
            tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0),
            tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
            tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0)
            ])
        
        tableView.register(HistoryTVCell.self, forCellReuseIdentifier: HistoryTVCell.identifier)
        return tableView
    }()
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        initView()
        initBinding()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.controller.start()
        
        
        //testUI()
    }

    deinit {
        print("shun - deinit")
    }
    
    // MARK: - Private Func
    private func initView(){
        self.title = "History"
    }
    
    private func initBinding() {
        self.viewModel.sectionViewModels.addObserver(fireNow: true, {
            [weak self] (sectionViewModels) in
            self?.tableView.reloadData()
        })
    }
    
    // MARK: - Public Func
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    // MARK: - Test Func
//
//    private func testUI(){
//        #if DEBUG
//
//        let button = UIButton(frame: CGRect(x: 100, y: 400, width: 100, height: 50))
//        button.backgroundColor = .blue
//        button.setTitle("In", for: .normal)
//        button.addTarget(self, action: #selector(buttonInAction), for: .touchUpInside)
//
//        self.view.addSubview(button)
//
//        let button2 = UIButton(frame: CGRect(x: 200, y: 400, width: 100, height: 50))
//        button2.backgroundColor = .red
//        button2.setTitle("Out", for: .normal)
//        button2.addTarget(self, action: #selector(buttonOutAction), for: .touchUpInside)
//
//        self.view.addSubview(button2)
//        #endif
//    }
//
//    @objc func buttonInAction(sender: UIButton!) {
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.DidEnterRegion.rawValue), object: nil)
//    }
//
//    @objc func buttonOutAction(sender: UIButton!) {
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RootMPLocationManagerServiceEnum.Notification.DidExitRegion.rawValue), object: nil)
//    }
//
    
}


// MARK: - UITableViewDataSource
extension HistoryVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionViewModel = self.viewModel.sectionViewModels.value[section]
        return sectionViewModel.rowViewModels.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.sectionViewModels.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionViewModel = viewModel.sectionViewModels.value[indexPath.section]
        let rowViewModel = sectionViewModel.rowViewModels[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: HistoryTVCell.identifier, for: indexPath)
        
        if let cell = cell as? HistoryTVCell{
            cell.setup(viewModel: rowViewModel )
        }
        
        return cell
    }
}
// MARK: - UITableViewDelegate
extension HistoryVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let sectionViewModel = self.viewModel.sectionViewModels.value[indexPath.section]
        if let rowViewModel = sectionViewModel.rowViewModels[indexPath.row] as? RowViewModelPressible {
            rowViewModel.cellPressed?()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = SectionHeaderView()
        let sectionViewModel = self.viewModel.sectionViewModels.value[section]
        view.setTitle(sectionViewModel.headerTitle)
        return view
    }

}
