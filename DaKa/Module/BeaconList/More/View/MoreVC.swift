//
//  MoreVC.swift
//  DaKa
//
//  Created by Shun on 2018/12/25.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit

class MoreVC: UIViewController {
    // MARK: - Type Alias
    
    // MARK: - Property
    
    lazy var versionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(label)
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 40.0),
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0.0)
            ])
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 12.0)
        return label
    }()
    
    lazy var moreProximitySettingView: MoreProximitySettingView = {
        let view = MoreProximitySettingView.init(frame: CGRect.init(x: 0.0,
                                                                    y: 60.0,
                                                                    width: UIScreen.main.bounds.width,
                                                                    height: 40.0))
        return view
    }()

    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        initView()
    }
    
    deinit {
        print("shun - deinit")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    // MARK: - Private Func
    private func initView(){
        
        if let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String {
            self.versionLabel.text = "Verson:  \(version)"
        }
        
        self.view.addSubview(moreProximitySettingView)
    }
    
    private func initBinding() {

    }
    
    // MARK: - Public Func
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}
