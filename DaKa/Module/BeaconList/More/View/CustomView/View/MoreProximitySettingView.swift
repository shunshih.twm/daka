//
//  MoreProximitySettingView.swift
//  DaKa
//
//  Created by Shun on 2018/12/27.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit

class MoreProximitySettingView: UIView {
    // MARK: - Type Alias
    
    // MARK: - Property
    
    var viewModel: MoreProximitySettingViewModel {
        return self.controller.viewModel
    }
    
    lazy var controller: MoreProximitySettingViewController = {
        return MoreProximitySettingViewController()
    }()
    

    private var ranges: [MPLocationManagerProximityEnum] = {
        return ( 0 ... 4 ).map({
            return MPLocationManagerProximityEnum(rawValue: $0)!
        })
    }()
    
    private lazy var rangesLabels: [UILabel] = {
        return ranges.enumerated().map({
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(label)
            label.font = UIFont.systemFont(ofSize: 12)
            label.text = String(describing: $1)
            label.tag = $0
            label.textColor = UIColor.gray
            label.textAlignment = .center
            label.isUserInteractionEnabled = true
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.labelPressed))
            label.addGestureRecognizer(tap)
            
            return label
        })
    }()
    
    private lazy var selectedViews: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        view.backgroundColor = UIColor.init(red: 0/255, green: 121/255, blue: 236/255, alpha: 1)
        return view
    }()
    
    private let spacing: CGFloat = 5.0
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    @objc
    func labelPressed(sender: UITapGestureRecognizer) {
        guard let index = sender.view?.tag,
            let _enum = MPLocationManagerProximityEnum(rawValue: index) else{
                return
        }
        
        self.controller.saveProximity(status: _enum)
        self.viewModel.proximity.value = _enum
    }
    
    // MARK: - Constructor
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInitialView()
        setupConstrint()
        initBinding()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInitialView()
        setupConstrint()
        initBinding()

    }
    
    
    // MARK: - Life Cycle
    
    override func didAddSubview(_ subview: UIView) {
        super.didAddSubview(subview)
        self.controller.start()
    }
    
    deinit {
        print("shun - deinit")
    }
    
    // MARK: - Private Func
    private func initBinding() {
        self.viewModel.proximity.addObserver (fireNow: false) {
            [weak self] (mPLocationManagerProximityEnum) in
            
            DispatchQueue.main.async {
               self?.updateUI(status: mPLocationManagerProximityEnum)
            }

        }
        
    }
    
    private func setupInitialView() {
        self.backgroundColor = UIColor.white
    }
    
    private func setupConstrint() {
        var constraint = [NSLayoutConstraint]()
        
        let perWidth: CGFloat = ( self.frame.width - ( self.spacing * CGFloat(self.rangesLabels.count + 1 ) ) ) / CGFloat( self.rangesLabels.count )
        
        for (index, label) in self.rangesLabels.enumerated() {
            let leftView = index == 0 ? self : self.rangesLabels[ index - 1]
            let rightView = index == self.rangesLabels.count - 1 ? self : self.rangesLabels[ index + 1 ]
            let leftAnchor = index == 0 ? self.leftAnchor : leftView.rightAnchor
            let rightAnchor = index == self.rangesLabels.count - 1 ? self.rightAnchor : rightView.leftAnchor
            
            constraint.append(label.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing))
            constraint.append(label.topAnchor.constraint(equalTo: self.topAnchor, constant: 0))
            constraint.append(label.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing))
            constraint.append(label.heightAnchor.constraint(equalToConstant: 40.0))
            constraint.append(label.widthAnchor.constraint(equalToConstant: perWidth))
        }
        
        // selectedViews
        constraint.append(selectedViews.widthAnchor.constraint(equalToConstant: perWidth))
        constraint.append(selectedViews.heightAnchor.constraint(equalToConstant: 2.0))
        constraint.append(selectedViews.leftAnchor.constraint(equalTo:  self.rangesLabels[0].leftAnchor, constant: 0.0))
        constraint.append(selectedViews.rightAnchor.constraint(equalTo:  self.rangesLabels[0].rightAnchor, constant: 0.0))
        constraint.append(selectedViews.topAnchor.constraint(equalTo: self.rangesLabels[0].bottomAnchor, constant: -5))
        
        
        NSLayoutConstraint.activate(constraint)
    }
    
    private func updateUI(status: MPLocationManagerProximityEnum){

        let index = status.rawValue
        
        UIView.animate(withDuration: 0.4,
                       delay: 0.0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 1,
                       options: [.curveEaseInOut],
                       animations: {
                        [weak self] in
                        
                        guard let _self = self else{
                            return
                        }
                        
                        var frame = _self.selectedViews.frame
                        frame.size.width = _self.rangesLabels[index].frame.origin.x + _self.rangesLabels[index].frame.width - _self.spacing
                        _self.selectedViews.frame = frame

                        
            }, completion: {
                (finished) in
                
                if finished{
                   
                }
        })
        
    }
    
    // MARK: - Public Func
    
}
