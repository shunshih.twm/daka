//
//  MoreProximitySettingViewModel.swift
//  DaKa
//
//  Created by Shun on 2018/12/27.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import Foundation

class MoreProximitySettingViewModel {
    let proximity = Observable<MPLocationManagerProximityEnum>(value: .All)
}
