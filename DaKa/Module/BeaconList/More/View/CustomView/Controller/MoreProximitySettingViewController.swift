//
//  MoreProximitySettingViewController.swift
//  DaKa
//
//  Created by Shun on 2018/12/27.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import Foundation

class MoreProximitySettingViewController {
    // MARK: - Type Alias
    
    // MARK: - Property
    let viewModel: MoreProximitySettingViewModel
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    init(viewModel: MoreProximitySettingViewModel = MoreProximitySettingViewModel()) {
        self.viewModel = viewModel
    }
    
    // MARK: - Life Cycle
    deinit {
        print("shun - deinit")
    }
    
    // MARK: - Private Func
    
    // MARK: - Public Func
    func start(){
        if let _statusRawValue = UserDefaultsHelper.shared.get(for: .More_Current_Proximity),
            let status = MPLocationManagerProximityEnum(rawValue: _statusRawValue){
            self.viewModel.proximity.value = status
        }else{
            saveProximity(status: .All)
            self.viewModel.proximity.value = .All
        }
    }
    
    func saveProximity(status: MPLocationManagerProximityEnum){
        UserDefaultsHelper.shared.set(status.rawValue, for: .More_Current_Proximity)
    }
}
