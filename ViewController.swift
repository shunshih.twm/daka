//
//  ViewController.swift
//  DaKa
//
//  Created by Shun on 2018/12/24.
//  Copyright © 2018 com.terntek. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // MARK: - Type Alias
    
    // MARK: - Property
    
    // MARK: - Accessor
    
    // MARK: - Outlet
    
    // MARK: - Action
    
    // MARK: - Constructor
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    deinit {
        print("shun - deinit")
    }
    
    // MARK: - Private Func
    
    // MARK: - Public Func
   
    
}

